# js-stylecheck

## Example GitLab-CI

```
teststyle:
  image: registry.gitlab.com/hpries/js-stylecheck:latest
  stage: test
  script:
    - standard "components/**/*.js" "pages/**/*.js"
```